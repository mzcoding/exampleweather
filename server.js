var http = require('http');
var https = require("https");

var s = http.createServer(function(request, response) {
  console.log(response);
  https.get('https://www.metaweather.com/api/location/search/?query=Mo', (resp) => {
  let data = '';
  resp.on('data', (chunk) => {
    data += chunk;
  });
  resp.on('end', () => {
     console.log(JSON.parse(data).explanation);
  });
  }).on("error", (err) => {
    console.log("Error: " + err.message);
  });
});

s.listen(8080);
