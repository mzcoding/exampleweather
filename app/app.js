var express = require('express');
var app = express();

const request = require('request-promise');

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/', function(req, res) {
  res.send('move to /search');
});
app.get('/search', function (req, res) {
  if(req.query.q) {
    q = req.query.q;
  }else{
    q = 'Moscow';
  }
  let url = "https://www.metaweather.com/api/location/search/?query=" + q;
  request(url)
    .then(response => {
     res.send(response);
 } ).catch(error => {
     console.log(error);
 });
});
app.get('/woeid', function(req, res) {
  if(req.query.q) {
    q = req.query.q;
  }else{
    q = 44418;
  }
  let url = "https://www.metaweather.com/api/location/" + q;
  request(url)
    .then(response => {
     res.send(response);
 } ).catch(error => {
     console.log(error);
 });
});

app.listen(3000, function () {});
